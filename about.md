---
title: Sobre
date: 2017-11-01T03:00:00.000+00:00
banner_image: "/uploads/2019/08/15/miguel.jpg"
heading: Sobre Penteado, Miguel
sub_heading: Áreas de interesse
layout: landing-page
textline: |-
  * Ciência de Dados
  * Datasets
  * Mapeamento Vetorial Tridimensional
publish_date: 2017-12-01T04:00:00.000+00:00
show_staff: false
hero_button:
  text: LinkedIn
  href: https://www.linkedin.com/in/miguel-penteado-760486a9/
menu:
  navigation:
    identifier: _about
    weight: 2
  footer:
    identifier: _about
    weight: 3

---
